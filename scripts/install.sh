#!bin/bash

#------------------------
# CREATE FOLDER STRUCTURE
#------------------------
# Configuration
mkdir /opt/config
mkdir /opt/config/grafana
mkdir /opt/config/mosquitto
mkdir /opt/config/telegraf
mkdir /opt/config/timescaledb

# Data
mkdir /opt/data
mkdir /opt/data/grafana
mkdir /opt/data/mosquitto
mkdir /opt/data/pgadmin
mkdir /opt/data/timescaledb

# Logs
mkdir /opt/log
mkdir /opt/log/mosquitto
mkdir /opt/log/timescaledb
mkdir /opt/log/controller
mkdir /opt/log/grafana

# -------------------------
# INSTALL REQUIRED PACKAGES
# -------------------------


# ---------------
# MANAGE SECURITY
# ---------------


# -------------
# INSTALL NRF24
# -------------


echo 'Done'
