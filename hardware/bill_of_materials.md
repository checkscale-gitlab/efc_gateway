# Bill of Materials
v0.1.0

| Part name | Part number | Amount | Unit price | Link | Notes |
| --------- | ----------- | ------ | ---------- | ---- | ----- |
| Raspberry Pi 3B | 1 | 1 |  |  |  |
| EFC HAT | 1 | 1 |  |  |  |
| nrf24L01+ | 2 | 1 | € 1,60 | [aliexpres](https://nl.aliexpress.com/item/2015078229.html) |  |
| 3.3 voltage regulator SMD | AMS1117 | 1 | € 0,06 | [aliexpres](http://aliexpress.com) |  |
| Capacitor 100uF SMD| 4 | 1 | € 0,10 | [aliexpres](https://nl.aliexpress.com/item/33013972369.html) |  |
| LED green SMD | 4 | 2 | € 0,04 | [aliexpres](https://nl.aliexpress.com/item/32825796287.html) |  |
| LED red SMD | 1 | 1 | € 0,04 | [aliexpres](https://nl.aliexpress.com/item/32825796287.html) |  |
